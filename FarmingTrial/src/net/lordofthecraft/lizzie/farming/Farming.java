package net.lordofthecraft.lizzie.farming;

import java.util.Random;

import net.lordofthecraft.lizzie.farming.listener.FarmingListener;

import org.bukkit.plugin.java.JavaPlugin;

public class Farming  extends JavaPlugin {

	private static Farming INSTANCE;
	
	private static FarmingListener listener;
	private static FarmingHandler handler;
	
	private static Random random;
	
	public Farming() {
		INSTANCE = this;
		random = new Random();
	}
	
	@Override
	public void onEnable() {
		
		handler = new FarmingHandler(INSTANCE);
		
		listener = new FarmingListener(INSTANCE);
		
		this.getServer().getPluginManager().registerEvents(listener, INSTANCE);
		
	}

	public static Farming getInstance() {
		return INSTANCE;
	}

	public FarmingHandler getHandler() {
		return handler;
	}

	public Random getRandom() {
		return random;
	}

}
