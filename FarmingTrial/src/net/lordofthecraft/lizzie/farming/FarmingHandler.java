package net.lordofthecraft.lizzie.farming;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import net.lordofthecraft.lizzie.farming.enums.ToolType;
import net.lordofthecraft.lizzie.farming.objects.Farmable;
import net.lordofthecraft.lizzie.farming.objects.Tool;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import de.diddiz.LogBlock.Actor;
import de.diddiz.LogBlock.Consumer;
import de.diddiz.LogBlock.LogBlock;

public class FarmingHandler {

	private final Farming plugin;
	private final HashMap<Material, Farmable> farmables;
	private final Random r;
	private final Consumer consumer;
	
	public FarmingHandler(Farming plugin) {
		
		this.plugin = plugin;
		
		r = plugin.getRandom();
		
		this.farmables = Maps.newHashMap();
		
		if (this.plugin.getServer().getPluginManager().getPlugin("LogBlock") != null) {
			this.consumer = LogBlock.getInstance().getConsumer();
		} else {
			this.consumer = null;
		}
		
		ToolType hoe = ToolType.HOE;
		ToolType axe = ToolType.AXE;
		
		Farmable wheat = new Farmable(Material.CROPS, Material.WHEAT, Material.SEEDS, hoe, 7, true);
		farmables.put(wheat.getBlock(), wheat);
		
		Farmable carrot = new Farmable(Material.CARROT, Material.CARROT_ITEM, null, hoe, 7, true);
		farmables.put(carrot.getBlock(), carrot);
		
		Farmable potato = new Farmable(Material.POTATO, Material.POTATO_ITEM, null, hoe, 7, true);
		farmables.put(potato.getBlock(), potato);
		
		Farmable wart = new Farmable(Material.NETHER_WARTS, Material.NETHER_STALK, null, hoe, 3, true);
		farmables.put(wart.getBlock(), wart);
		
		Farmable pumpkin = new Farmable(Material.PUMPKIN, Material.PUMPKIN, null, axe, 0, false);
		farmables.put(pumpkin.getBlock(), pumpkin);
		
		Farmable melon = new Farmable(Material.MELON_BLOCK, Material.MELON, null, axe, 0, false);
		farmables.put(melon.getBlock(), melon);
		
		Farmable pumpkinstem = new Farmable(Material.PUMPKIN_STEM, null, Material.PUMPKIN_SEEDS, hoe, 7, true);
		farmables.put(pumpkinstem.getBlock(), pumpkinstem);
		
		Farmable melonstem = new Farmable(Material.MELON_BLOCK, null, Material.MELON_SEEDS, hoe, 7, true);
		farmables.put(melonstem.getBlock(), melonstem);
		
		ItemStack snakeDrop = new ItemStack(Material.MUTTON, 2);
		ItemMeta meta = snakeDrop.getItemMeta();
		meta.setDisplayName("Raw Snake Meat");
		snakeDrop.setItemMeta(meta);

		this.SNAKE_DROP = snakeDrop;
		
		ItemStack deadCrop = new ItemStack(Material.DEAD_BUSH, 1);
		meta = deadCrop.getItemMeta();
		meta.setDisplayName("Infected Crop");
		List<String> lore = Lists.newArrayList();
		lore.add(ChatColor.DARK_GRAY + "These crops look infected, perhaps hazardous.");
		meta.setLore(lore);
		deadCrop.setItemMeta(meta);

		this.DEAD_CROP = deadCrop;
	}

	public Farmable getFarmable(Block b) {
		if (!farmables.containsKey(b.getType())) return null;
		return farmables.get(b.getType());
	}

	public void breakLogCancel(Cancellable e, Block b, Player p) {
		e.setCancelled(true);
		if (this.consumer != null) {
			consumer.queueBlockBreak(new Actor(p.getName(), p.getUniqueId()), b.getState());
		}
		b.setType(Material.AIR);
	}

	public Tool getTool(ItemStack is) {
		return new Tool(is);
	}

	public boolean dropExtraSeeds() {
		return r.nextInt(3) == 0;
	}

	public ItemStack damageTool(ItemStack toDamage, int damage, Location l) {
		ItemStack is = toDamage.clone();
		if (is.getDurability() + damage >= is.getType().getMaxDurability()) {
			l.getWorld().playSound(l, Sound.ITEM_BREAK, 1, 1);
			return new ItemStack(Material.AIR);
		} else {
			is.setDurability((short) (is.getDurability() + damage));
			return is;
		}
	}

	public Location getMiddle(Location l) {
		return l.add(0.5, 0.5, 0.5);
	}
	
	public void notPlayerBreak(Block b) {
		Farmable f = getFarmable(b);

		if (f != null && f.dropsExtra()) {
			b.setType(Material.AIR);
			Location l = getMiddle(b.getLocation());
			World w = l.getWorld();
			if (f.hasSeeds()) w.dropItemNaturally(l, new ItemStack(f.getSeed(), dropExtraSeeds() && f.isGrown(b) ? 2 : 1));
		}
	}
	
	public boolean shouldRunEvent (Farmable f) {
		return (f.dropsExtra() && r.nextInt(1000) == 0);
	}
	
	private final ItemStack SNAKE_DROP;
	private final ItemStack DEAD_CROP;
	
	public void runEvent(Player p, Block b) {
		switch(r.nextInt(3)) {
		case 0: new SnakeRunnable(p).runTask(plugin); break;
		case 1: new PomRunnable(p, b).runTask(plugin); break;
		case 2: new DeadRunnable(p, b).runTask(plugin); break;
		}
	}

	private class SnakeRunnable extends BukkitRunnable {

		private final Player p;

		public SnakeRunnable (Player p) {
			this.p = p;
		}

		@Override
		public void run() {
			alert(p, "A wild snake attacks you, though you manage to fight it off!");
			p.damage(4);
			p.playSound(p.getEyeLocation(), Sound.CREEPER_HISS, 1f, 2f);
			drop(p.getLocation(), SNAKE_DROP);
		}

	}

	private class PomRunnable extends BukkitRunnable {

		private final Player p;
		private final Block b;

		public PomRunnable (Player p, Block b) {
			this.p = p;
			this.b = b;
		}

		@Override
		public void run() {
			alert(p, "Hmm... that appears to be a beet growing -- wait, no! POMEGRENADE!");
			p.damage(4.0D);
			p.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 1f, 1f);
			p.setVelocity(p.getLocation().toVector().subtract(b.getLocation().toVector()).setY(0).normalize().multiply(2.5).setY(0.75D));
		}
		
	}
	
	private class DeadRunnable extends BukkitRunnable {

		private final Player p;
		private final Block b;

		public DeadRunnable (Player p, Block b) {
			this.p = p;
			this.b = b;
		}

		@SuppressWarnings("deprecation")
		@Override
		public void run() {
			alert(p, "That crop appears to be infected...");
			p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 0, false, false));
			Block change = b.getRelative(BlockFace.DOWN);
			if (change.getType() == Material.SOIL) {
				change.setType(Material.DIRT);
				change.setData((byte) 2);
			}
			drop(getMiddle(b.getLocation()), DEAD_CROP);
		}
		
	}

	private void alert(Player p, String msg) {
		p.sendMessage(ChatColor.DARK_PURPLE + "" + ChatColor.ITALIC + msg);

	}

	private void drop(Location l, ItemStack i) {
		l.getWorld().dropItemNaturally(l, i);
	}
}
