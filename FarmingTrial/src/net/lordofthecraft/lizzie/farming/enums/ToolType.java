package net.lordofthecraft.lizzie.farming.enums;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

public enum ToolType {
	
	HOE(Lists.newArrayList(
			Material.WOOD_HOE, 
			Material.GOLD_HOE, 
			Material.STONE_HOE, 
			Material.IRON_HOE, 
			Material.DIAMOND_HOE)),
			
	AXE(Lists.newArrayList(
			Material.WOOD_AXE, 
			Material.GOLD_AXE, 
			Material.STONE_AXE, 
			Material.IRON_AXE, 
			Material.DIAMOND_AXE));
	
	private final ArrayList<Material> m;
	
	ToolType(ArrayList<Material> m) {
		this.m = m;
	}
	
	public ArrayList<Material> getTools() {
		return m;
	}
	
	public static ToolType get(ItemStack itemStack) {
		if (itemStack == null) return null;
		Material mat = itemStack.getType();
		for (ToolType t : ToolType.values()) {
			if (t.getTools().contains(mat)) {
				return t;
			}
		}
		return null;
	}
}