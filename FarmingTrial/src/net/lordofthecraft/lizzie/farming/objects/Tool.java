package net.lordofthecraft.lizzie.farming.objects;

import net.lordofthecraft.lizzie.farming.Farming;
import net.lordofthecraft.lizzie.farming.enums.ToolType;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class Tool {
	
	private int fortuneLevel = 0;
	private int materialLevel = 0;
	private int unbreakingLevel = 0;
	private final ToolType type;
	
	public Tool(ItemStack is) {
		if (is.containsEnchantment(Enchantment.LOOT_BONUS_BLOCKS)) fortuneLevel = is.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
		if (is.containsEnchantment(Enchantment.DURABILITY)) unbreakingLevel = is.getEnchantmentLevel(Enchantment.DURABILITY);
		switch (is.getType()) {
		case WOOD_HOE : case WOOD_AXE : case STONE_HOE : case STONE_AXE : this.materialLevel = 1; break;
		case GOLD_HOE : case GOLD_AXE : case IRON_HOE : case IRON_AXE : this.materialLevel = 2; break;
		case DIAMOND_HOE : case DIAMOND_AXE : this.materialLevel = 3; break;
		default:
			break;
		}
		this.type = ToolType.get(is);
	}
	
	public int getDropAmount(Farmable f) {
		return (f.getTool() != type) ? 0 : f.dropsExtra() ? this.fortuneLevel + this.materialLevel : 1;
	}
	
	public int getDamageAmount(Farmable f) {
		if (f.getTool() != type) return 2;
		else if (this.unbreakingLevel == 0) return 1;
		else {
			int damage = Farming.getInstance().getRandom().nextInt(this.unbreakingLevel + 1);
			return (damage >= 1) ? 0 : 1;
		}
	}
}
