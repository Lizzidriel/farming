package net.lordofthecraft.lizzie.farming.listener;

import java.util.List;

import net.lordofthecraft.lizzie.farming.Farming;
import net.lordofthecraft.lizzie.farming.FarmingHandler;
import net.lordofthecraft.lizzie.farming.objects.Farmable;
import net.lordofthecraft.lizzie.farming.objects.Tool;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.inventory.ItemStack;

public class FarmingListener implements Listener {

	private FarmingHandler handler;

	public FarmingListener(Farming plugin) {
		this.handler = plugin.getHandler();
	}

	@EventHandler (priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockBreak(BlockBreakEvent e) {

		Player p = e.getPlayer();

		if (!p.getGameMode().equals(GameMode.SURVIVAL)) return;

		Block b = e.getBlock();

		Farmable f = handler.getFarmable(b);
		if (f != null) {
			ItemStack is = p.getItemInHand();

			if (!(is == null || is.getType() == Material.AIR)) {

				Tool t = handler.getTool(is);
				Location l = handler.getMiddle(b.getLocation());
				World w = l.getWorld();

				if (handler.shouldRunEvent(f)) {
					handler.runEvent(p, b);
				} else {
					if (f.hasDrops() && f.isGrown(b)) {
						int amount = t. getDropAmount(f);
						if (amount > 0) w.dropItemNaturally(l, new ItemStack(f.getDrop(), amount));
					}
					if (f.hasSeeds()) {
						w.dropItemNaturally(l, new ItemStack(f.getSeed(), handler.dropExtraSeeds() && f.isGrown(b) ? 2 : 1));
					} else if (!f.isGrown(b)) {
						w.dropItemNaturally(l, new ItemStack(f.getDrop(), 1));
					}
				}

				int damage = t.getDamageAmount(f);
				if (damage > 0) p.setItemInHand(handler.damageTool(is, damage, l));
			}
			
			handler.breakLogCancel(e, b, p);
			
		} else {

			handler.notPlayerBreak(b.getRelative(BlockFace.UP));

		}
	}

	@EventHandler (ignoreCancelled = true)
	public void onBlockFromTo(BlockFromToEvent e) {

		Block b = e.getToBlock();

		handler.notPlayerBreak(b);

	}

	@EventHandler (ignoreCancelled = true)
	public void onBlockPistonExtend(BlockPistonExtendEvent e) {

		List<Block> bs = e.getBlocks();

		for (Block b : bs) {

			handler.notPlayerBreak(b);

		}	
	}

	@EventHandler (ignoreCancelled = true)
	public void onBlockPhysics(BlockPhysicsEvent e) {

		Block b = e.getBlock();

		Farmable f = handler.getFarmable(b);

		Block down = b.getRelative(BlockFace.DOWN);

		if (f != null && f.dropsExtra() && !(down.getType() == Material.SOUL_SAND || down.getType() == Material.SOIL)) {
			b.setType(Material.AIR);
			Location l = handler.getMiddle(b.getLocation());
			World w = l.getWorld();
			if (f.hasSeeds()) w.dropItemNaturally(l, new ItemStack(f.getSeed(), handler.dropExtraSeeds() && f.isGrown(b) ? 2 : 1));
		}	
	}
}
