package net.lordofthecraft.lizzie.farming.objects;

import net.lordofthecraft.lizzie.farming.enums.ToolType;

import org.bukkit.Material;
import org.bukkit.block.Block;

public class Farmable {

	private final Material block;
	private final Material drop;
	private final Material seeds;
	private final ToolType tool;
	private int grown;
	private boolean dropExtra;

	public Farmable (Material block, Material drop, Material seeds, ToolType tool, int grown, boolean dropExtra) {
		this.block = block;
		this.drop = drop;
		this.seeds = seeds;
		this.tool = tool;
		this.grown = grown;
		this.dropExtra = dropExtra;
	}

	public Material getBlock() {
		return block;
	}

	public Material getDrop() {
		return drop;
	}

	public Material getSeed() {
		return seeds;
	}

	public ToolType getTool() {
		return tool;
	}

	public boolean needsTool() {
		return tool != null;
	}

	public boolean hasSeeds() {
		return seeds != null;
	}

	public boolean hasDrops() {
		return drop != null;
	}

	@SuppressWarnings("deprecation")
	public boolean isGrown(Block b) {
		return (b.getData() >= grown);
	}

	public boolean dropsExtra() {
		return dropExtra;
	}

}
